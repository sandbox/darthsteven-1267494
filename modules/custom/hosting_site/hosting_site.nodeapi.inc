<?php
// $Id$

/**
 * @file Site nodeapi implementations.
 */

/**
 * Implementation of hook_node_info
 */
function hosting_site_node_info() {
  $types["site"] =  array(
    "base" => 'hosting_site',
    "name" => t('Site'),
    "has_title" => TRUE,
    "title_label" => t('Domain name'),
    "description" => hosting_node_help("site"),
  );
  return $types;
}

/**
 * Implementation of hook_view().
 */
function hosting_site_view(&$node, $teaser = false) {

  $node->content['info']['#prefix'] = '<div id="hosting-site-info">';
  if ($node->site_status == HOSTING_SITE_ENABLED) {
    $node->content['info']['link'] = array(
      '#markup' => _hosting_site_goto_link($node),
      '#weight' => -10
    );
  }

  if (is_numeric($node->client)) {
    $node->content['info']['client'] = array(
      '#type' => 'item',
      '#title' => t('Client'),
      '#markup' => _hosting_node_link($node->client),
      '#weight' => 5
    );
  }

  $node->content['info']['verified'] = array(
    '#type' => 'item',
    '#title' => t('Verified'),
    '#markup' => hosting_format_interval($node->verified),
  );

  $node->content['info']['platform'] = array(
    '#type' => 'item',
    '#title' => t('Platform'),
    '#markup' => _hosting_node_link($node->platform),
  );

  if ($node->profile) {
    $node->content['info']['profile'] = array(
      '#type' => 'item',
      '#title' => t('Install profile'),
      '#markup' => _hosting_node_link($node->profile),
    );
  }
  if ($node->site_language) {
    $node->content['info']['site_language'] = array(
      '#type' => 'item',
      '#title' => t('Language'),
      '#markup' => _hosting_language_name($node->site_language),
    );
  }
  if ($node->db_server) {
    $node->content['info']['db_server'] = array(
      '#type' => 'item',
      '#title' => t('Database server'),
      '#markup' => _hosting_node_link($node->db_server),
    );
  }

  if ($node->nid) {
    $node->content['info']['status'] = array(
      '#type' => 'item',
      '#title' => t('Status'),
      '#markup' => _hosting_site_status($node),
    );
  }

  $node->content['info']['#suffix'] = '</div>';



  if ($node->nid) {
    $node->content['tasks_view'] = array(
      '#type' => 'item',
      '#markup' => hosting_task_table($node),
      '#prefix' => '<div id="hosting-task-list">',
      '#suffix' => '</div>',
      '#weight' => 10
    );
  }
  return $node;
}


/**
 * Implementation of hook_load().
 *
 * @param node
 *    Node object
 */
function hosting_site_load($nodes) {
  //$additions = db_query('SELECT  client, db_server, platform, profile, language as site_language, last_cron, cron_key, status AS site_status, verified FROM {hosting_site} WHERE vid = :vid', array(':vid' => $node->vid))->fetchAssoc();
  foreach ($nodes as &$node) {
    $additions = array(
      'platform' => 2,
      'db_server' => 3,
      'profile' => 4,
      'client' => 5,
      'verified' => REQUEST_TIME - rand(0, 100000),
      'last_cron' => REQUEST_TIME - rand(0, 100000),
      'site_status' => rand(-2, 1),
      'site_language' => '',
    );

    foreach ($additions as $k => $v) {
      $node->$k = $v;
    }
  }

}
