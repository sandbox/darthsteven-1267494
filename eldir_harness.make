core = 7.x
api = 2

; Contrib projects (alphabetical order)
; ==============================================================================

projects[admin_menu][subdir] = "contrib"
projects[conditional_styles][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[ctools][subdir] = "contrib"

projects[devel][subdir] = "contrib"

projects[features][subdir] = "contrib"

projects[overlay_paths][subdir] = "contrib"

projects[strongarm][subdir] = "contrib"

projects[eldir][subdir] = "contrib"
projects[eldir][type] = "theme"
projects[eldir][download][type] = "git"


; Libraries (alphabetical order please)
; ==============================================================================
