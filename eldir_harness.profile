<?php

/**
* drush make profiles/eldir_harness/eldir_harness_stub.make --working-copy -y
*/

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function eldir_harness_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = 'Eldir Harness';

  // Set the default country to be the UK.
  $form['server_settings']['site_default_country']['#default_value'] = 'GB';

  if (defined('SITE_ENVIRONMENT') && SITE_ENVIRONMENT == 'DEV') {
    $form['site_information']['site_mail']['#default_value']    = 'info@example.com';
    $form['admin_account']['account']['name']['#default_value'] = 'admin';
    $form['admin_account']['account']['mail']['#default_value'] = 'info@example.com';
    $form['admin_account']['account']['pass'] = array(
      '#type' => 'value',
      '#value' => 'admin'
    );
    $form['update_notifications']['update_status_module']['#default_value'] = array(0,0);
  }
}

/**
 * Implements hook_install_tasks().
 */
function eldir_harness_install_tasks() {
  $tasks = array();

  $tasks['eldir_harness_dev_task'] = array('display_name' => st('Development extras'));

  $tasks['eldir_harness_theme_task'] = array();

  return $tasks;
}

function eldir_harness_theme_task() {
  $enable = array(
    'theme_default' => 'eldir',
  );

  theme_enable($enable);

  foreach ($enable as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }

  // Disable the default Bartik theme
  theme_disable(array('bartik'));
}


function eldir_harness_dev_task() {
  // Install some modules while developing
  module_enable(array('devel'));
}
