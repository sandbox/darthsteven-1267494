core = 7.x
api = 2

projects[drupal][type] = core

projects[eldir_harness][type] = profile
projects[eldir_harness][download][type] = git
projects[eldir_harness][download][url] = http://git.drupal.org/sandbox/darthsteven/1267494.git
projects[eldir_harness][download][branch] = 7.x-1.x
